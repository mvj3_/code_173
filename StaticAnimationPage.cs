using System.Windows.Navigation;

namespace SimpleAnimationApp
{
    public partial class StaticAnimationPage
    {
        public StaticAnimationPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            staticAnimation.Begin();
        }
    }
}